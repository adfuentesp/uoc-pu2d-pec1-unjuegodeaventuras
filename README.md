# [PU2D] PEC 1 - Un juego de aventuras.

Este juego ha sido realizado para la asignatura **Programación en Unity 2D** del **Máster en Diseño y desarrollo de videojuegos** de la **Universitat Oberta de Catalunya**.

## Unfriendly Paradise

Unfriendly Paradise es un videojuego de aventuras que recrea la estrategia del duelo de insultos de Monkey Island. El juego se ambienta en una tribu en la que nuestro protagonista, *Red head*, debe enfrentarse en duelo a diversos integranes de la tribu, en los que saldrá victorioso el primero que consiga ganar tres asaltos.

## Cómo jugar

En la pantalla de inicio podremos comenzar una partida y cambiar la dificultad de la misma. Cada dificultad significará un duelo contra un enemigo diferente.

Una vez se inicie la partida el primer turno será escogido de forma aleatoria, y decidirá quien comienza insultando. Una vez se haya pronunciado el insulto, el otro jugador (el usuario o el enemigo) deberá responderle correctamente. Si la respuesta es acertada el punto del asalto será para el jugador que respondió, y en caso responder incorrectamente, el punto del asalto será del jugador insultante. A partir de este momento, el jugador que comienza cada asalto como insultante será aquel que ganó el asalto anterior. Ganará el primer jugador que consiga tres asaltos ganados.

El usuario dispondrá en cada uno de sus turnos (ya sea como insultante o cómo el jugador que responde) de un panel con los diferentes insultos/respuestas. Este panel se encuentra paginado, y podrá utilizar las flechas de navegación para moverse por las diferentes páginas y encontrar el insulto/respuesta necesario.
El enemigo interactuará siempre de manera aleatoria a la hora de lanzar insultos y de responder.

En la parte inferior derecha se encuentran unos paneles con la información de los asaltos de la partida. Allí podremos observar cuantos asaltos lleva ganados cada jugador mediante unos indicadores que se van rellenando conforme avanza la partida.

Una vez haya finalizado el juego, podremos volver a intentar la partida, o regresar al inicio para modificar la dificultad y probarte a duelo con otro enemigo.

Además, durante la partida el usuario podrá utilizar la tecla de escape ESC para pausar la partida. Mientras se encuentra pausada puede reanudarla, regresar al inicio o salir de la misma.

## Desarrollo del videojuego

A continuación se explicará la estructura y desarrollo llevabo a cabo para la realización del videojuego:

### **Insultos / Respuestas**. 

Son leidos y cargados desde un fichero .json alojado en la carpeta */Resources* que tiene la siguiente estructura:  

```
{
	"insultos": [
		{
			"texto": "¡Llevarás mi espada como si fueras un pincho moruno!",
			"respuesta": "Primero deberías dejar de usarla como un plumero."
		},
		{
			...
		}
	]
}
```

Esta información luego se almacen de la siguiente manera: 

```
public class Datos
{
	public List<Insulto> insultos;
	public List<Respuesta> respuestas;
}

public class Insulto
{
	public string texto;
	public Respuesta respuesta;
}

public class Respuesta
{
	public string texto;
}
```

Cada objeto respuesta de cada insulto, es el mismo objeto en memoria que los que conforman la lista de respuestas. Se ha diseñado así para poder disponer de los listados de insultos y respuestas al mismo tiempo que podemos obtener de forma rápida la respuesta correcta de un insulto como objeto individual, ocupando el mínimo espacio en memoria.

### Máquina de estados. 

El ciclo del videojuego se basa en una máquina de cuatro estados. 

Los estados de la máquina siguen la siguiente estructura:

```
private class Estado
{
	public delegate void Accion();
	public Accion accion;
	public Estado transicion;
}
```

El controlador de la partida almacena en cada momento el estado actual en el que se encuentra, y mediante la función delegada y las transiciones es capaz de correr el ciclo completo del juego.

Los cuatro estados que forman la máquina son los siguientes:

* Inicio. Determina de forma aleatoria quién comienza la partida.
* Primera ronda. Primera parte del asalto en la que interviene el insultante.
* Segunda ronda. Segunda parte del asalto en la que interviene el jugador que responde.
* Fin de rondas. Resolución del asalto en la que se comprueba si se da la condición de victoria para algun jugador.

Las transiciones entre los estados son:

* Inicio -> Primera ronda -> Segunda ronda -> Fin de rondas -> Primera ronda

La transición Fin de rondas -> Primera ronda sólo ocurre si no se ha llegado a ninguna condición de victoria para algún jugador.

### Dificultad

La dificultad está centrada en la elección de respuestas por parte del enemigo, y está diseñada mediante una combinación de aleatoriedad y dificultad.

* Dificultad facil. Las respuestas del enemigo son siempre aleatorias.
* Dificultad media. El enemigo hace una tirada aleatoria entre 0 y 99. Si obtiene un valor menor a 65 (65% de probabilidad) este responderá con la respuesta correcta.
* Dificultad dificil. El enemigo hace una tirada aleatoria entre 0 y 99. Si obtiene un valor menor a 85 (85% de probabilidad) este responderá con la respuesta correcta.

### Coroutines

Se ha hecho uso de coroutines para generar tiempos de espera entre turnos, para así simular el pensamiento del enemigo y darle una interacción más real a la partida. Además también se han utilizado para establecer el tiempo que duran determinadas animaciones que ocurre cuando cada jugador interacciona con la partida.

## En funcionamiento

A continuación se muestra un video de ejemplo de funcionamiento del juego.

https://www.youtube.com/watch?v=nXseUPyU9Q0