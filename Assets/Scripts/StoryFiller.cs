﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryFiller {

	[Serializable]
	private class DatosJson
	{
		public InsultoJson[] insultos;
	}

	[Serializable]
	private class InsultoJson
	{
		public string texto;
		public string respuesta;
	}

	private static string ficheroDatos = "datos.json";

	/* Lee la información de un archivo JSON y lo devuelve parseado en unos datos */
	public static GameplayManager.Datos FillStory()
	{
		GameplayManager.Datos datos = new GameplayManager.Datos();
		List<GameplayManager.Insulto> insultos = new List<GameplayManager.Insulto>();
		List<GameplayManager.Respuesta> respuestas = new List<GameplayManager.Respuesta>();
		// Se carga el json
		TextAsset json = Resources.Load<TextAsset>(ficheroDatos.Replace(".json",""));
		if (json == null)
		{
			Debug.LogError("No se ha podido cargar los datos en formato json");
		} 
		else
		{
			DatosJson datosJson = JsonUtility.FromJson<DatosJson>(json.text);
			foreach(InsultoJson insulto in datosJson.insultos)
			{
				// Se crean los insultos y respuestas
				GameplayManager.Respuesta r = crearRespuesta(insulto.respuesta);
				respuestas.Add(r);
				GameplayManager.Insulto i = crearInsulto(insulto.texto, r);
				insultos.Add(i);
			}
		}
		// Se devuelven los datos
		datos.insultos = insultos;
		datos.respuestas = respuestas;
		return datos;
	}

	/* Crea una respuesta en el formato de dato del GameplayManager */
	private static GameplayManager.Respuesta crearRespuesta(string texto)
	{
		GameplayManager.Respuesta respuesta = new GameplayManager.Respuesta();
		respuesta.texto = texto;
		return respuesta;
	}

	/* Crea un insulto en el formato de dato del GameplayManager */
	private static GameplayManager.Insulto crearInsulto(string texto, GameplayManager.Respuesta respuesta)
	{
		GameplayManager.Insulto insulto = new GameplayManager.Insulto();
		insulto.texto = texto;
		insulto.respuesta = respuesta;
		return insulto;
	}
}
