﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoPaginacion : MonoBehaviour {

	public void ReproducirSonido()
	{
		// Se establece el audio source en el parent y no 
		// en el hijo (el boton) para poder desactivarlo
		// sin tener que esperar a que el audio termine
		gameObject.GetComponent<Transform>().parent
			.GetComponent<AudioSource>().Play();
	}
}
