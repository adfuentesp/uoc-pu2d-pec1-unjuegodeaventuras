﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Personaliza el enemigo contra el que nos enfrentamos dependiendo de la dificultad
 */ 
public class SceneDetails : MonoBehaviour {

	private static string NOMBRE_GUARDIAN = "GUARDIAN";
	private static string NOMBRE_CENTAURO = "CENTAUR";
	private static string NOMBRE_CABALLO = "DARK HORSE";

	[SerializeField]
	private GameObject pjEnemigo;

	[SerializeField]
	private Sprite guardian;

	[SerializeField]
	private Sprite guardianAvatar;

	[SerializeField]
	private Sprite centaur;

	[SerializeField]
	private Sprite centaurAvatar;

	[SerializeField]
	private Sprite darkHorse;

	[SerializeField]
	private Sprite darkHorseAvatar;

	[SerializeField]
	private Text nombreEnemigo;

	[SerializeField]
	private GameObject avatarEnemigo;

	void Start()
	{
		Image image = pjEnemigo.GetComponent<Image>();
		switch (DatosPartida.Dificultad)
		{
			case 0:
				// El guardian representa el enemigo facil
				image.sprite = guardian;
				if (avatarEnemigo != null)
				{
					Image avatar = avatarEnemigo.GetComponent<Image>();
					avatar.sprite = guardianAvatar;
					nombreEnemigo.text = NOMBRE_GUARDIAN;
				}
				break;
			case 1:
				// El centauro representa el enemigo intermedio
				image.sprite = centaur;
				if (avatarEnemigo != null)
				{
					Image avatar = avatarEnemigo.GetComponent<Image>();
					avatar.sprite = centaurAvatar;
					nombreEnemigo.text = NOMBRE_CENTAURO;
				}
				break;
			case 2:
				// El caballo negro representa el enemigo dificil
				image.sprite = darkHorse;
				if (avatarEnemigo != null)
				{
					Image avatar = avatarEnemigo.GetComponent<Image>();
					avatar.sprite = darkHorseAvatar;
					nombreEnemigo.text = NOMBRE_CABALLO;
				}
				break;
			default:
				// Por defecto se representa al guardián
				image.sprite = guardian;
				if (avatarEnemigo != null)
				{
					Image avatar = avatarEnemigo.GetComponent<Image>();
					avatar.sprite = guardianAvatar;
					nombreEnemigo.text = NOMBRE_GUARDIAN;
				}
				break;
		}
	}
}
