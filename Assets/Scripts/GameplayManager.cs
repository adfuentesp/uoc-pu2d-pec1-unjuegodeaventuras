﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * Controla la ejecución del juego
 */
public class GameplayManager : MonoBehaviour {

	/*
	 * DATOS DE INSULTOS Y RESPUESTAS
	 */ 
	public class Datos
	{
		public List<Insulto> insultos;
		public List<Respuesta> respuestas;
	}

	public class Insulto
	{
		public string texto;
		public Respuesta respuesta;
	}

	public class Respuesta
	{
		public string texto;
	}

	private Datos datos;

	/*
	 * MAQUINA DE ESTADOS
	 */ 
	private class Estado
	{
		public delegate void Accion();
		public Accion accion;
		public Estado transicion;
	}

	private Estado estadoActual;

	/* Configura los estados de la maquina, y devuelve el primero de ellos */
	private Estado ConfigurarMaquinaEstados()
	{
		Estado inicio = new Estado();
		inicio.accion = () =>
		{
			// El turno al inicio de la partida es aleatorio
			comienzaRondaJugador = Random.Range(0, 2).Equals(0);
			AvanzarPorTransicion();
		};

		Estado primeraRonda = new Estado();
		primeraRonda.accion = () =>
		{
			ComenzarRonda();
		};

		Estado segundaRonda = new Estado();
		segundaRonda.accion = () =>
		{
			StartCoroutine(SiguienteTurno());
		};

		Estado finRonda = new Estado();
		finRonda.accion = () =>
		{
			StartCoroutine(FinalizarRonda());
		};

		// Transiciones
		inicio.transicion = primeraRonda;
		primeraRonda.transicion = segundaRonda;
		segundaRonda.transicion = finRonda;
		finRonda.transicion = primeraRonda;

		// Se devuelve el estado de inicio
		return inicio;
	}

	/* Avanza del estado actual al siguiente, y ejecuta su accion */
	private void AvanzarPorTransicion()
	{
		estadoActual = estadoActual.transicion;
		if (estadoActual.accion != null)
		{
			estadoActual.accion();
		}
	}

	/*
	 * TIEMPOS DE ESPERAS PARA COROUTINES Y ANIMACIONES
	 */ 
	private const float TIEMPO_ESPERA = 2.0f;
	private const float TIEMPO_ANIMACION = 1.0f;
	private const string P_ANIMACION_ACCION = "estaActuando";

	// Puntos con los que empieza cada jugador
	private int puntosJugador = 0;
	private int puntosOrdenador = 0;
	
	// Determina la persona que lanza el primer insulto de la ronda
	private bool comienzaRondaJugador;
	// Insulto de la ronda
	private Insulto current;

	// Paginacion de acciones
	private const float SEPARACION_ACCIONES = 18f;
	private int tamanoPagina = 4;
	private int paginaActual = 0;

	// Puntuacion para alcanzar la victora
	[SerializeField]
	private int puntosVictoria = 3;

	// Prefab que se genera en la pantalla y representa una accion del jugador (insulto/respuesta)
	[SerializeField]
	private Button accionJugador;

	// Puntuacion del jugador en pantalla
	[SerializeField]
	private Image[] puntuacionJugador = new Image[3];
	
	private Color32 colorPuntoJugador = new Color32(228, 113, 113, 255);

	// Puntuacion del ordenador en pantalla
	[SerializeField]
	private Image[] puntuacionOrdenador = new Image[3];
	
	private Color32 colorPuntoOrdenador = new Color32(225, 199, 138, 255);

	// Burbujas de dialogos
	[SerializeField]
	private GameObject burbujaJugador;

	[SerializeField]
	private GameObject burbujaOrdenador;

	// Panel donde se instancian los prefabs de acciones
	[SerializeField]
	private GameObject panelAccionesJugador;

	// Botones para la paginación
	[SerializeField]
	private Button retrocederPagina;

	[SerializeField]
	private Button avanzarPagina;

	// Personajes de la pantalla
	[SerializeField]
	private GameObject pjJugador;

	[SerializeField]
	private GameObject pjOrdenador;

	// Panel de pausa
	[SerializeField]
	private GameObject panelPausa;

	// Camara principal 
	[SerializeField]
	private Camera camara;

	public void Start()
	{
		datos = StoryFiller.FillStory();
		estadoActual = ConfigurarMaquinaEstados();
		if (estadoActual.accion != null)
		{
			estadoActual.accion();
		}
	}

	/*
	 * INICIO DE RONDA
	 */ 

	/* Inicio de cada ronda */
	private void ComenzarRonda()
	{
		DesactivaBurbujasTexto();
		if (comienzaRondaJugador)
		{
			IniciaRondaJugador();
		}
		else
		{
			IniciaRondaOrdenador();
		}
	}

	/* Inicia la ronda el jugador */
	private void IniciaRondaJugador()
	{
		paginaActual = 0;
		RellenarAccionesConInsultos();
		ConfigurarBotonesPaginacion(datos.insultos.Count, RellenarAccionesConInsultos);
	}

	/* Inicia la ronda el ordenador*/
	private void IniciaRondaOrdenador()
	{
		current = obtenerInsultoAleatorio();
		burbujaOrdenador.SetActive(true);
		burbujaOrdenador.GetComponentInChildren<Text>().text = current.texto;
		StartCoroutine(lanzarAnimacion(pjOrdenador));
		AvanzarPorTransicion();
	}

	/*
	 * SEGUNDO TURNO DE RONDA
	 */
	
	/* Siguiente turno. Espera unos segundos para simular la interacción */
	private IEnumerator SiguienteTurno()
	{
		yield return new WaitForSeconds(TIEMPO_ESPERA);
		if (comienzaRondaJugador)
		{
			ContinuaRondaOrdenador();
		}
		else
		{
			ContinuaRondaJugador();
		}
		yield return null;
	}

	/* Continua la ronda el jugador */
	private void ContinuaRondaJugador()
	{
		paginaActual = 0;
		RellenarAccionesConRespuestas();
		ConfigurarBotonesPaginacion(datos.respuestas.Count, RellenarAccionesConRespuestas);
	}

	/* Continua la ronda el ordenador */
	private void ContinuaRondaOrdenador()
	{
		// La dificultad ajusta el nivel de probabilidad de que el ordenador acierte
		Respuesta respuesta = obtenerRespuestaAleatoria();
		switch (DatosPartida.Dificultad)
		{
			case 0:
				// La probabilidad de acierto es totalmente aleatoria				
				break;
			case 1:
				// La probabilidad de acierto es del 65%
				if (Random.Range(0, 100) < 65)
				{
					respuesta = current.respuesta;
				}
				break;
			case 2:
				// La probabilidad de acierto es del 85%
				if (Random.Range(0, 100) < 85)
				{
					respuesta = current.respuesta;
				}
				break;
			default:
				// La probabilidad de acierto es totalmente aleatoria
				break;
		}
		burbujaOrdenador.SetActive(true);
		burbujaOrdenador.GetComponentInChildren<Text>().text = respuesta.texto;
		if (respuesta.Equals(current.respuesta))
		{
			GanaRondaOrdenador();
		}
		else
		{
			GanaRondaJugador();
		}
		StartCoroutine(lanzarAnimacion(pjOrdenador));
		AvanzarPorTransicion();
	}

	/* Obtiene una respuesta aleatoria */
	private Respuesta obtenerRespuestaAleatoria()
	{
		return datos.respuestas[Random.Range(0, datos.respuestas.Count)];
	}

	/*
	 * FINALIZAR RONDA
	 */ 

	/* Finaliza la ronda y comprueba la condicion de victoria */
	private IEnumerator FinalizarRonda()
	{
		yield return new WaitForSeconds(TIEMPO_ESPERA);
		ComprobarCondicionVictoria();
		AvanzarPorTransicion();
		yield return null;
	}

	/*
	 * FUNCIONES DE SOPORTE AL CICLO DEL JUEGO
	 */

	/* Rellena de forma paginada el panel de acciones del usuario con los insultos*/
	private void RellenarAccionesConInsultos()
	{
		LimpiarPanelAccionesJugador();
		bool primeraAccion = true;
		float x = 0.0f;
		float y = 0.0f;
		int numeroElementos = 0;
		Transform parent = panelAccionesJugador.GetComponent<Transform>();
		for (int i = paginaActual * tamanoPagina; (i < datos.insultos.Count && numeroElementos < tamanoPagina); i++)
		{
			// Se instancia un botón de acción
			Button botonAccion = Instantiate(accionJugador, parent);
			// Se establece su posicion
			RectTransform trAccion = botonAccion.GetComponent<RectTransform>();
			if (primeraAccion)
			{
				x = trAccion.localPosition.x;
				y = trAccion.localPosition.y;
				primeraAccion = false;
			}
			else
			{
				y = y - SEPARACION_ACCIONES;
				trAccion.localPosition = new Vector3(x, y, 0.0f);
			}
			// Se establece el texto del insulto
			botonAccion.GetComponentInChildren<Text>().text = datos.insultos[i].texto;
			// Se le añade un listener al botón que contiene
			AnadirListenerBotonInsulto(botonAccion.GetComponent<Button>(), datos.insultos[i]);
			numeroElementos++;
		}
		ConfigurarVisibilidadPaginacion(datos.insultos.Count);
	}

	/* Añade los listenes a los botones con los insultos */
	private void AnadirListenerBotonInsulto(Button button, Insulto insulto)
	{
		button.onClick.AddListener(() =>
		{
			current = insulto;
			burbujaJugador.SetActive(true);
			burbujaJugador.GetComponentInChildren<Text>().text = current.texto;
			LimpiarPanelAccionesJugador();
			StartCoroutine(lanzarAnimacion(pjJugador));
			AvanzarPorTransicion();
		});
	}

	/* Rellena de forma paginada el panel de acciones del usuario con las respuestas */
	private void RellenarAccionesConRespuestas()
	{
		LimpiarPanelAccionesJugador();
		bool primeraAccion = true;
		float x = 0.0f;
		float y = 0.0f;
		int numeroElementos = 0;
		Transform parent = panelAccionesJugador.GetComponent<Transform>();
		for (int i = paginaActual * tamanoPagina; (i < datos.respuestas.Count && numeroElementos < tamanoPagina); i++)
		{
			// Se instancia un texto de acción
			Button botonAccion = Instantiate(accionJugador, parent);
			// Se establece su posicion
			RectTransform trAccion = botonAccion.GetComponent<RectTransform>();
			if (primeraAccion)
			{
				x = trAccion.localPosition.x;
				y = trAccion.localPosition.y;
				primeraAccion = false;
			}
			else
			{
				y = y - SEPARACION_ACCIONES;
				trAccion.localPosition = new Vector3(x, y, 0.0f);
			}
			// Se establece el texto de la respuesta
			botonAccion.GetComponentInChildren<Text>().text = datos.respuestas[i].texto;
			// Se establece el listener correspondiente
			AnadirListenerBotonRespuesta(botonAccion.GetComponent<Button>(), datos.respuestas[i]);
			numeroElementos++;
		}
		ConfigurarVisibilidadPaginacion(datos.respuestas.Count);
	}

	/* Añade los listenes a los botones con las respuestas */
	private void AnadirListenerBotonRespuesta(Button button, Respuesta respuesta)
	{
		button.onClick.AddListener(() =>
		{
			burbujaJugador.SetActive(true);
			burbujaJugador.GetComponentInChildren<Text>().text = respuesta.texto;
			if (respuesta.Equals(current.respuesta))
			{
				GanaRondaJugador();
			}
			else
			{
				GanaRondaOrdenador();
			}
			LimpiarPanelAccionesJugador();
			StartCoroutine(lanzarAnimacion(pjJugador));
			AvanzarPorTransicion();
		});
	}

	/* Lanza la animación de acción en el personaje informado durante un tiempo configurado */
	private IEnumerator lanzarAnimacion(GameObject personaje)
	{
		Animator animator = personaje.GetComponent<Animator>();
		animator.SetBool(P_ANIMACION_ACCION, true);
		yield return new WaitForSeconds(TIEMPO_ANIMACION);
		animator.SetBool(P_ANIMACION_ACCION, false);
	}

	/* Habilita y desahabilita los botones de avanzar o retroceder dependiendo de la pagina en la que estemos */
	private void ConfigurarVisibilidadPaginacion(int total)
	{
		retrocederPagina.gameObject.SetActive(paginaActual > 0);
		avanzarPagina.gameObject.SetActive(((paginaActual + 1) * tamanoPagina) < total);
	}

	/* Obtiene un insulto aleatorio */
	private Insulto obtenerInsultoAleatorio()
	{
		return datos.insultos[Random.Range(0, datos.insultos.Count)];
	}

	/* Establece el punto ganado del ordenador, asi como la interacción con la pantalla del suceso */
	private void GanaRondaOrdenador()
	{
		puntosOrdenador++;
		puntuacionOrdenador[puntosOrdenador - 1].color = colorPuntoOrdenador;
		puntuacionOrdenador[puntosOrdenador - 1].GetComponent<Transform>().parent
			.GetComponent<AudioSource>().Play();
		comienzaRondaJugador = false;
	}

	/* Establece el punto ganado del jugador, asi como la interacción con la pantalla del suceso */
	private void GanaRondaJugador()
	{
		puntosJugador++;
		puntuacionJugador[puntosJugador - 1].color = colorPuntoJugador;
		puntuacionJugador[puntosJugador - 1].GetComponent<Transform>().parent
			.GetComponent<AudioSource>().Play();
		comienzaRondaJugador = true;
	}
	
	/* Determina si se da la condición de victoria para el jugador o el ordenador */
	private void ComprobarCondicionVictoria()
	{		
		bool hayCondicionVictoria = false;
		if (puntosVictoria.Equals(puntosJugador))
		{
			DatosPartida.Victoria = true;
			hayCondicionVictoria = true;
		}
		else if (puntosVictoria.Equals(puntosOrdenador))
		{
			DatosPartida.Victoria = false;
			hayCondicionVictoria = true;
		}
		if (hayCondicionVictoria)
		{
			SceneManager.LoadScene("Final");
		}
	}

	/* Elimina todos los hijos del panel de acciones del jugador */
	private void LimpiarPanelAccionesJugador()
	{
		Transform parent = panelAccionesJugador.GetComponent<Transform>();
		foreach (Transform child in parent)
		{
			Destroy(child.gameObject);
		}		
	}

	/* Desactiva las burbujeas de texto del jugador y el ordenador*/
	private void DesactivaBurbujasTexto()
	{
		burbujaJugador.SetActive(false);
		burbujaOrdenador.SetActive(false);
	}

	/* Establece el comportamiento que siguen los botones de paginacion al pulsarlos.
	 * Recibe como parámetro la función a realizar (rellenar acciones con insultos o con respuestas)
	 * tras ajustar la pagina actual en la que nos encontramos */
	private void ConfigurarBotonesPaginacion(int tamanoMaximo, System.Action rellenarAcciones)
	{
		retrocederPagina.onClick.RemoveAllListeners();
		retrocederPagina.onClick.AddListener(() =>
		{
			if (paginaActual > 0)
			{
				paginaActual--;
				rellenarAcciones();
			}
		});
		avanzarPagina.onClick.RemoveAllListeners();
		avanzarPagina.onClick.AddListener(() =>
		{
			if ((paginaActual + 1) < (tamanoMaximo / tamanoPagina))
			{
				paginaActual++;
				rellenarAcciones();
			}
		});
	}

	/* Comprueba continuamente si se ha pulsado la tecla de Escape para interactuar con el menu de pausa */
	public void Update()
	{
		if (Input.GetKeyUp(KeyCode.Escape))
		{
			panelPausa.SetActive(!panelPausa.activeSelf);
			ControlDeAudioPausado();
		}
	}

	/* Permite cerrar el panel de pausa desde un botón y no solo pulsando Escape */
	public void CerrarPanelPausa()
	{
		panelPausa.SetActive(false);
		ControlDeAudioPausado();
	}
	
	/* Detiene / Vuelve a reproducir el audio que suena en la camara principal */
	private void ControlDeAudioPausado()
	{
		AudioSource audioSource = camara.GetComponent<AudioSource>();
		if (audioSource.isPlaying)
		{
			audioSource.Pause();
		}
		else
		{
			audioSource.UnPause();
		}
	}
}
