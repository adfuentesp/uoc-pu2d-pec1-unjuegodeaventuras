﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Controlador de la dificultad de la partida
 */
public class Difficulty : MonoBehaviour {

	[SerializeField]
	private Dropdown target;

	void Start()
	{
		// El valor del combo sérá igual al configurado en la partida
		target.value = DatosPartida.Dificultad;
	}

	/* Cambia la dificultad de la partida al cambiarse en el dropdown */
	public void CambiarDificultad()
	{
		DatosPartida.Dificultad = target.value;
	}
}
