﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Final : MonoBehaviour {

	[SerializeField]
	private Text resultado;

	private static string MENSAJE_GANADOR = "YOU\nWIN";
	private static string MENSAJE_PERDEDOR = "YOUR ENEMY\nWINS";

	public void Start()
	{
		if (DatosPartida.Victoria)
		{
			resultado.text = MENSAJE_GANADOR;
		}	
		else
		{
			resultado.text = MENSAJE_PERDEDOR;
		}
	}
}
