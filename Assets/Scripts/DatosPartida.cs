﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Datos sobre la partida que persiste entre escenas
 */
public static class DatosPartida {

	// Determina la victoria por parte del jugador
	public static bool Victoria { get; set; }

	// Determina la dificultad de la partida
	public static int Dificultad = 0;
}
